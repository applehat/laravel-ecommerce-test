<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = array_map('str_getcsv', file(__DIR__.'/data/products.csv'));
        $keys = null;
        foreach ($csv as $i => $row) {
            if ($keys) {
                // Insert rows from CSV
                $build = [];
                foreach ($row as $index => $value) {
                    $build[$keys[$index]] = $value;
                }
                DB::table('products')->insert($build);
            } else {
                // First row contains the keys for the columns
                // Probably a smarter way of handling this, but it works.
                $keys = $row;
            }
        }
    }
}
