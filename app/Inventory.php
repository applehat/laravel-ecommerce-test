<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = 'inventory';

    public function product() 
    {
        return $this->belongsTo('App\Product');
    }

    public function orders() 
    {
        return $this->hasMany('App\Orders');
    }

    public function getPriceDollarsAttribute()
    {
        return number_format($this->price_cents / 100, 2);
    }

    public function getCostDollarsAttribute()
    {
        return number_format($this->cost_cents / 100, 2);
    }
}
