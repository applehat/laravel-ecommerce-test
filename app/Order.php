<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function product() 
    {
        return $this->belongsTo('App\Product');
    }

    public function inventory() 
    {
        return $this->belongsTo('App\Inventory');
    }

    public function getTotalDollarsAttribute()
    {
        return number_format($this->total_cents / 100, 2);
    }

    public function getTrackingNumberLinkAttribute()
    {
        switch ($this->shipper_name)
        {
            case "USPS":
                $link = "https://tools.usps.com/go/TrackConfirmAction?tLabels=" . $this->tracking_number;
                break;
            case "UPS":
                $link = "https://www.ups.com/track?loc=en_US&tracknum=" . $this->tracking_number;
                break;
            case "DHL":
                $link = "https://www.dhl.com/en/express/tracking.html?brand=DHL&AWB=" . $this->tracking_number;
                break;
            case "FedEx":
                $link = "https://www.fedex.com/apps/fedextrack/?tracknumbers=" . $this->tracking_number;
                break;
            default:
                $link = null;
        }
        return $link;
    }
}
