<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InventoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $inv = $request->user()->inventory();
        $total_on_hand = $inv->sum('quantity');
        $unique_items = $inv->count();

        $inventory = $inv->orderBy('product_name');

        $filter = null;
        $below = null;

        if ($request->input('filter')) {
            $filter = $request->input('filter');
            $inventory = $inventory->where('product_name','LIKE','%'.$filter.'%')->orWhere('SKU','LIKE','%'.$filter.'%');
        }

        if ($request->input('below')) {
            $below = $request->input('below');
            $inventory = $inventory->where('quantity', '<', $below);

        }
        $inventory = $inventory->paginate(20);
        
        return view('inventory', [
            'inventory' => $inventory,
            'total_on_hand' => $total_on_hand,
            'unique_items' => $unique_items,
            'filter' => $filter,
            'below' => $below
        ]);
    }
}
