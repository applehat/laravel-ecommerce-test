<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $o = $request->user()->orders();
        $total_sales = $o->sum('total_cents');
        $total_orders = $o->count();
        $average_per_sale =  number_format(($total_sales / 100) / $total_orders, 2);
        $total_sales_dollars = number_format($total_sales / 100, 2);
        $orders = $o->paginate(20);
        $grouped = $request->user()->orders()->groupBy('order_status')->selectRaw('count(*) as total, order_status')->get();

        return view('orders', ['orders' => $orders, 'total_sales' => $total_sales_dollars, 'average_per_sale' => $average_per_sale,'grouped'=>$grouped]);
    }
}
