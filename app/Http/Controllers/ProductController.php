<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product as Product; 

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $product_id)
    {
        $user_id = $request->user()->id;
      
        $product = Product::where([
            ['id','=',$product_id],   
            ['admin_id','=',$user_id]
        ])->first();
  
        return view('product', ['product' => $product]);
    }
}
