<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    public function inventory() 
    {
        return $this->hasMany('App\Inventory');
    }

    public function orders() 
    {
        return $this->hasMany('App\Orders');
    }

    public function getShippingPriceDollarsAttribute()
    {
        return number_format($this->shipping_price / 100, 2);
    }
}
