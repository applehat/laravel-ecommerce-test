@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Inventory</div>
                <div class="card-body">
        
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  Total Unique Items
                                </div>
                                <div class="card-body">
                                    {{ $unique_items }} 
                                </div>
                              </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                  Total Items On Hand:
                                </div>
                                <div class="card-body">
                                    {{ $total_on_hand }}
                                </div>
                              </div>
                        </div>
                    </div>
                    <hr />
                    
                    <div class="d-flex flex-row-reverse">
                        <form class="form-inline">
                            <div class="form-group mb-3">
                                <input type="text" class="form-control  form-control-sm" name="filter" placeholder="Product or SKU" value="{{ $filter ? $filter : "" }}">
                            </div>
                            <div class="form-group mx-sm-1 mb-2">
                            <button type="submit" class="btn btn-primary btn-sm mb-2">Search</button>
                            </div>
                            
                            <div class="form-group mx-sm-1 mb-2">
                            <a href="{{ route('inventory', ['below' => 10]) }}" class="btn btn-warning btn-sm mb-2">Show Low Inventory</a>
                            </div>
                            @if($filter or $below)
                            <div class="form-group mx-sm-1 mb-2">
                            <a href="{{ route('inventory') }}" class="btn btn-light btn-sm mb-2">Clear</a>
                            </div>
                            @endif

                        </form>
                    </div>

                    @if($filter)
                        {{ $inventory->appends(['filter'=>$filter])->links() }}
                    @elseif($below)
                        {{ $inventory->appends(['below'=>$below])->links() }}
                    @else
                        {{ $inventory->links() }}
                    @endif

                    <table class="table table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th>Product Name</th>
                                <th>SKU</th>
                                <th>Brand</th>
                                <th>Quantity</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($inventory as $inv)
                                <tr>        
                                    
                                    <td><a href="{{ route('product.show', ['product_id' => $inv->product->id]) }}">{{ $inv->product->product_name }}</a></td>
                                    <td>{{ $inv->sku }}</td>
                                    <td>{{ $inv->product->brand }}</td>
                                    <td class="@if($inv->quantity < 1) table-danger @elseif ($inv->quantity < 10) table-warning @endif">{{ $inv->quantity }}</td>
                                    <td>{{ $inv->color }}</td>
                                    <td>{{ $inv->size }}</td>
                                    <td>${{ $inv->price_dollars }}</td>
                                    <td>${{ $inv->cost_dollars }}</td>
                                
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    @if($filter)
                        {{ $inventory->appends(['filter'=>$filter])->links() }}
                    @elseif($below)
                        {{ $inventory->appends(['below'=>$below])->links() }}
                    @else
                        {{ $inventory->links() }}
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
