@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Orders</div>
                <div class="card-body">
                    
                    <div class="card">
                        <div class="card-header">
                          Sales Information
                        </div>
                        <div class="card-body">
                            <div class="d-flex justify-content-around">
                                    @foreach ($grouped as $group)
                                        <div>{{ $group->order_status }} Orders  <span class="badge">{{ $group->total }}</span></div>
                                    @endforeach
                                    <div>Averge Sale <span class="badge">${{ $average_per_sale }}</span></div>
                                    <div>Total Sales <span class="badge">${{ $total_sales }}</span></div>
                            </div>   
                        </div>
                    </div>
                   
                    <hr />

                    {{ $orders->links() }}
                    <table class="table table-bordered table-small">
                        <thead class="table-dark">
                            <tr>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Product Name</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Order Status</th>
                                <th>Order Total</th>
                                <th>Shipper</th>
                                <th>Tracking Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>        
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->email }}</td>
                                    <td><a href="{{ route('product.show', ['product_id' => $order->product->id]) }}">{{ $order->product->product_name }}</a></td>
                                    <td>{{ $order->inventory->color }}</td>
                                    <td>{{ $order->inventory->size }}</td>
                                    <td>{{ $order->order_status }}{{ $order->transaction_id !== "NULL" ? " (" . $order->transaction_id . ")" : "" }}</td>
                                    <td>${{ $order->total_dollars }}</td>
                                    <td>{{ $order->shipper_name }}</td>
                                    <td>
                                        @if ($order->tracking_number_link)
                                            <a href="{{ $order->tracking_number_link }}" target="_blank">{{ $order->tracking_number }}</a>
                                        @else
                                            {{ $order->tracking_number }}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
