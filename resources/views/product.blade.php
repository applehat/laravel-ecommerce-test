@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
            @if ($product)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Product Info</div>
                    <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label for="productName">Product Name</label>
                                    <input type="text" class="form-control" id="productName" value="{{ $product->product_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" id="description" value="{{ $product->description }}">
                                </div>
                                <div class="form-group">
                                    <label for="style">Style</label>
                                    <input type="text" class="form-control" id="style" value="{{ $product->style }}">
                                </div>
                                <div class="form-group">
                                    <label for="brand">brand</label>
                                    <input type="text" class="form-control" id="brand" value="{{ $product->brand }}">
                                </div>
                                <div class="form-group">
                                    <label for="url">url</label>
                                    <input type="text" class="form-control" id="url" value="{{ $product->url }}">
                                </div>
                                <div class="form-group">
                                    <label for="product_type">product_type</label>
                                    <input type="text" class="form-control" id="product_type" value="{{ $product->product_type }}">
                                </div>
                                <div class="form-group">
                                    <label for="shipping_price">shipping_price</label>
                                    <input type="text" class="form-control" id="shipping_price" value="{{ $product->shipping_price }}">
                                </div>
                                <div class="form-group">
                                    <label for="note">note</label>
                                    <input type="text" class="form-control" id="note" value="{{ $product->note }}">
                                </div>
                                <button type="submit" class="btn btn-primary" disabled>Save</button>
                            </form>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Inventory</div>
                    <div class="card-body">
                        <table class="table table-bordered table-small">
                            <thead class="table-dark">
                                <tr>
                                    <th>sku</th>
                                    <th>quantity</th>
                                    <th>color</th>
                                    <th>size</th>
                                    <th>price</th>
                                    <th>cost</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($product->inventory as $inv) 
                                <tr>   
                                    <td>{{ $inv->sku }}</td>
                                    <td class="@if($inv->quantity < 1) table-danger @elseif ($inv->quantity < 10) table-warning @endif">{{ $inv->quantity }}</td>
                                    <td>{{ $inv->color }}</td>
                                    <td>{{ $inv->size }}</td>
                                    <td>${{ $inv->price_dollars }}</td>
                                    <td>${{ $inv->cost_dollars }}</td>
                            @endforeach
                            </tbody>
                            </table>
                    </div>
                </div>
            </div>
            @else
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    Sorry, we can not find the specified product.
                </div> 
            </div>     
            @endif
    </div>
</div>
@endsection
