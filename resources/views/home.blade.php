@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Products</div>
                <div class="card-body">
                    {{ $products->links() }}
                    <table class="table table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th>Name</th>
                                <th>Style</th>
                                <th>Brand</th>
                                <th>SKUs</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>        
                                    <td><a href="{{ route('product.show', ['product_id' => $product->id]) }}">{{ $product->product_name }}</a></td>
                                    <td>{{ $product->style }}</td>
                                    <td>{{ $product->brand }}</td>
                                    <td>
                                        @foreach ($product->inventory as $inventory)
                                            <a class="btn btn-light" href="{{ route('inventory',['filter'=>$inventory->sku]) }}">{{ $inventory->sku }}</a>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
